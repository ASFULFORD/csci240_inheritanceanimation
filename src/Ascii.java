import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.*;

public class Ascii extends JFrame implements ActionListener
{
	
	private static final long serialVersionUID = 1L;
	
	//creating array to hold each frame
	int numberOfFrames = 10;
	int currentFrame = 0;
	String[] frames = new String[numberOfFrames];
	
	//creating button, label, and text area objects
	JLabel frameNumberLbl = new JLabel(Integer.toString(currentFrame, 10));
	JTextArea textArea =  new JTextArea(25, 50);
	JButton backBtn = new JButton("<");
	JButton firstFrameBtn = new JButton("<<");
	JButton forwardBtn = new JButton(">");
	JButton lastFrameBtn = new JButton(">>");
	JButton saveFrameBtn = new JButton("SAVE FRAME");
	JButton loadFromFileBtn = new JButton("LOAD FILE");
	JButton saveToFileBtn = new JButton("SAVE FILE");
	JButton playBtn = new JButton("PLAY");
		
	public static void main(String[] args)
	{
		new Ascii();
	}//end main
	public Ascii()
	{
		super("Anthony's Animation Station");
		setupGUI();
		registerListeners();
	}//end Ascii class
	public void setupGUI()
	{
		//creating panel objects
		JPanel pnlTextArea = new JPanel();
		JPanel pnlControls = new JPanel();
		//creating panels	
		pnlTextArea.setLayout(new FlowLayout());
		pnlControls.setLayout(new FlowLayout());
		//attaching button, label, and text objects to panels
		pnlTextArea.add(textArea);		
		pnlControls.add(frameNumberLbl);
		pnlControls.add(firstFrameBtn);
		pnlControls.add(backBtn);
		pnlControls.add(saveFrameBtn);
		pnlControls.add(forwardBtn);
		pnlControls.add(lastFrameBtn);
		pnlControls.add(playBtn);
		pnlControls.add(loadFromFileBtn);
		pnlControls.add(saveToFileBtn);
		//creating a container and attaching panels to it
		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(pnlTextArea, BorderLayout.CENTER);
		mainPanel.add(pnlControls, BorderLayout.SOUTH);
		
		this.setSize(1000,500);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}//end setupGUI method
	public void registerListeners()
	{
		//registering listeners to watch for button action
		backBtn.addActionListener(this);
		saveFrameBtn.addActionListener(this);
		playBtn.addActionListener(this);
		loadFromFileBtn.addActionListener(this);
		saveToFileBtn.addActionListener(this);
		forwardBtn.addActionListener(this);
		firstFrameBtn.addActionListener(this);
		lastFrameBtn.addActionListener(this);
	}//end registerListeners
	public void actionPerformed(ActionEvent e)
	{
		System.out.println(e.getActionCommand());
		//checking to see which buttons are pressed
		if(e.getSource() == backBtn)
		{
			back();
		}
		else if(e.getSource() == saveFrameBtn)
		{
			saveFrame();
		}
		else if(e.getSource() == playBtn)
		{
			play();
		}
		else if(e.getSource() == loadFromFileBtn)
		{
			loadFromFile();
		}
		else if(e.getSource() == saveToFileBtn)
		{
			saveToFile();
		}
		else if(e.getSource() == forwardBtn)
		{
			forward();
		}
		else if(e.getSource() == firstFrameBtn)
		{
			goToFirstFrame();
		}
		else if(e.getSource() == lastFrameBtn)
		{
			goToLastFrame();
		}
		else
		{
			frameNumberLbl.setText("Error");
		}
	}//end action performed
	public void back()
	{
		//move one frame back
		if(currentFrame == 0)
			currentFrame = 0;
		else
			currentFrame--;
		//then display frame
		displayFrame();
	}//end back()
	public void forward()
	{
		//move one frame forward
		if(currentFrame == numberOfFrames - 1)
			currentFrame = numberOfFrames - 1;
		else
			currentFrame++;
		//then display frame
		displayFrame();
	}//end forward()
	public void goToFirstFrame()
	{
		//set frame to beginning
		currentFrame = 0;
		//display frame
		displayFrame();
	}//end goToFirstFrame()
	public void goToLastFrame()
	{
		//set frame to end
		currentFrame = numberOfFrames - 1;
		//then display
		displayFrame();
	}//end goToLastFrame()
	public void saveFrame()
	{
		//pull text from frame and insert into array[frame number]
		frames[currentFrame] = textArea.getText();
	}//end saveFrame()
	public void play()//THIS FUNCTION DOESN'T WORK
	{
		//display each element of the array in a timely manner... in theory
		//set to beginning
		currentFrame = 0;
		while(currentFrame < numberOfFrames)
		{
			//try and catch used for Thread.sleep
			try
			{
				//add 250 millisecond delay
				Thread.sleep(250);
				//display frame
				displayFrame();
				//iterate frame number
				currentFrame++;
			}//end try
			catch(InterruptedException e)
			{
				System.out.println("awakened prematurely");
			}//end catch
		}//end while
	}//end play()
	public void loadFromFile()
	{
		//load animation from file
		try
		{
			//create new file input stream
			FileInputStream fIn = new FileInputStream("Animation.txt");
			//create new object input stream and pass into file input stream
			ObjectInputStream obIn = new ObjectInputStream(fIn);
			//pass array to object input stream
			frames = (String[])obIn.readObject();
			//pass object into file
			System.out.println(frames);
			//close file
			obIn.close();
		}//end try
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}//end catch
		goToFirstFrame();
	}//end loadFromFile()
	public void saveToFile()
	{
		//save animation to file
		try
		{
		//create file output stream
		FileOutputStream fo = new FileOutputStream("Animation.txt");
		//create new object output stream and pass into file input stream
		ObjectOutputStream obOut = new ObjectOutputStream(fo);
		//save the array to that object
		obOut.writeObject(frames);
		//close file
		obOut.close();
		}//end try
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}//end catch
	}//end saveToFile()
	public void displayFrame()
	{
		//change frame label
		frameNumberLbl.setText(Integer.toString(currentFrame, 1));
		//clear the text area
		textArea.setText(null);
		//pull text from desired array element
		String frameText = frames[currentFrame];
		//print it out
		textArea.insert(frameText, 0);
	}//end displayFrame()
}//end Ascii class
